package com.gohn.kakaomobility.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.gohn.kakaomobility.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, PhotoListActivity.class);
                intent.putExtra("type", v.getId());
                startActivity(intent);
            }
        };

        findViewById(R.id.btn_show_custom).setOnClickListener(listener);
        findViewById(R.id.btn_show_glide).setOnClickListener(listener);
    }
}
