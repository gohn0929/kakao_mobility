package com.gohn.kakaomobility.view;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gohn.kakaomobility.R;
import com.gohn.kakaomobility.data.ThumbnailItem;
import com.gohn.kakaomobility.etc.Constant;
import com.gohn.kakaomobility.etc.ImageLoader;

import java.util.List;

public class ThumbnailAdapter extends RecyclerView.Adapter<ThumbnailAdapter.ViewHolder> {

    private Activity activity;
    private List<ThumbnailItem> items;
    private ImageLoader imageLoader;
    private int type;

    public ThumbnailAdapter(Activity activity, int type, List<ThumbnailItem> items) {
        this.activity = activity;
        this.type = type;
        this.items = items;
        this.imageLoader = new ImageLoader(activity);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        View layout;
        ImageView thumbnail;

        public ViewHolder(View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.container);
            thumbnail = (ImageView) itemView.findViewById(R.id.img_thumbnail);
        }
    }

    @Override
    public ThumbnailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_thumbnail_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ThumbnailAdapter.ViewHolder holder, final int position) {
        if (imageLoader == null) return;

        final ThumbnailItem item = items.get(position);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, PhotoViewActivity.class);
                intent.putExtra("data", item);
                activity.startActivity(intent);
            }
        });

        holder.thumbnail.setImageBitmap(null);
        switch (type) {
            case R.id.btn_show_glide:
                Glide.with(activity).load(Constant.HOST + item.getResource()).into(holder.thumbnail);
                break;
            case R.id.btn_show_custom:
                imageLoader.display(holder.thumbnail, Constant.HOST + item.getResource());
                break;
            default:
                break;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public void setItems(List<ThumbnailItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}
