package com.gohn.kakaomobility.view;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.ethanhua.skeleton.RecyclerViewSkeletonScreen;
import com.ethanhua.skeleton.Skeleton;
import com.gohn.kakaomobility.R;
import com.gohn.kakaomobility.data.CollectionParseTask;
import com.gohn.kakaomobility.data.ThumbnailItem;
import com.gohn.kakaomobility.etc.Constant;

import java.util.ArrayList;
import java.util.List;

public class PhotoListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_list);

        final int type = getIntent().getIntExtra("type", 0);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        final ThumbnailAdapter adapter = new ThumbnailAdapter(PhotoListActivity.this, type, new ArrayList<ThumbnailItem>());
        recyclerView.setAdapter(adapter);

        // 타입에 따라 Glide를 쓰는지 커스텀을 쓰는지 타이틀에 표시
        switch (type) {
            case R.id.btn_show_glide:
                setActionBarTitle("List with Glide");
                break;
            case R.id.btn_show_custom:
                setActionBarTitle("List with Custom Image Loader");
                break;
            default:
                break;
        }

        // 스켈레톤 셋팅을 한다.
        final RecyclerViewSkeletonScreen skeletonScreen = Skeleton.bind(recyclerView)
                .adapter(adapter)
                .load(R.layout.layout_skeleton_thumbnail)
                .shimmer(true)
                .color(R.color.shimmerColor)
                .count(30)
                .frozen(true)
                .show();

        showProgress();
        new CollectionParseTask(Constant.HOST + Constant.COLLECTION_PATH, new CollectionParseTask.CollectionParseListener() {
            @Override
            public void onCollectionsParsed(List<ThumbnailItem> photoItems) {
                hideProgress();
                skeletonScreen.hide();

                // html 파싱이 끝나면 스켈레톤을 숨기고 어댑터를 통해 섬네일을 리스트 형태로 보여준다.

                if (photoItems == null) {
                    toast("Collection Parsing Fail or Empty");
                    return;
                }

                if (adapter != null) {
                    adapter.setItems(photoItems);
                }
            }
        }).execute();
    }
}
