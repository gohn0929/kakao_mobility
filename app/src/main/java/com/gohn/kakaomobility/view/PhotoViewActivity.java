package com.gohn.kakaomobility.view;

import android.os.Bundle;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.gohn.kakaomobility.R;
import com.gohn.kakaomobility.data.PhotoParseTask;
import com.gohn.kakaomobility.data.ThumbnailItem;
import com.gohn.kakaomobility.etc.Constant;

public class PhotoViewActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_view);

        ThumbnailItem item = (ThumbnailItem) getIntent().getSerializableExtra("data");
        if (item == null) {
            Toast.makeText(this, "유효하지 않은 이미지 입니다.", Toast.LENGTH_SHORT);
            finish();
            return;
        }

        setActionBarTitle(item.getTitle());

        showProgress();
        new PhotoParseTask(Constant.HOST + item.getLink(), new PhotoParseTask.PhotoParseListener() {
            @Override
            public void onPhotoParsed(String resource) {
                hideProgress();

                if ( resource == null) toast("유효하지 않은 이미지 주소 입니다.");

                PhotoView photoView = (PhotoView) findViewById(R.id.img_source);
                Glide.with(PhotoViewActivity.this).load(Constant.HOST + resource).into(photoView);
            }
        }).execute();
    }
}
