package com.gohn.kakaomobility.data;

import java.io.Serializable;

/**
 * Created by gohn on 2018. 4. 27..
 */

public class ThumbnailItem implements Serializable {
    public ThumbnailItem(String title, String link, String resource) {
        this.link = link;
        this.resource = resource;
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public String getResource() {
        return resource;
    }

    public String getTitle() {
        return title;
    }

    private String link;
    private String resource;
    private String title;

    @Override
    public String toString() {
        return "PhotoItem{" +
                "link='" + link + '\'' +
                ", resource='" + resource + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
