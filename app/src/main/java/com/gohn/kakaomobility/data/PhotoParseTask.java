package com.gohn.kakaomobility.data;

import android.os.AsyncTask;
import android.util.Log;

import com.gohn.kakaomobility.etc.Constant;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gohn on 2018. 4. 27..
 */

public class PhotoParseTask extends AsyncTask<String, Void, Document> {
    private String uriPath;
    private PhotoParseListener listener;

    public PhotoParseTask(String uriPath, PhotoParseListener listener) {
        this.uriPath = uriPath;
        this.listener = listener;
    }

    public interface PhotoParseListener {
        void onPhotoParsed(String resource);
    }

    @Override
    public void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    public Document doInBackground(String... params) {
        try {
            return Jsoup.connect(uriPath).get();
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void onPostExecute(Document document) {
        super.onPostExecute(document);

        if (document == null) {
            if (listener != null)
                listener.onPhotoParsed(null);
        }

        // 메인 이미지 클래스를 가져온다.
        Elements elements = document.getElementsByClass("innerImage");

        if (elements != null) {
            // 리스너로 이미지 주소를 넘긴다.
            if (listener != null)
                listener.onPhotoParsed(elements.first().attr("src"));
        } else {
            if (listener != null)
                listener.onPhotoParsed(null);
        }
    }
}
