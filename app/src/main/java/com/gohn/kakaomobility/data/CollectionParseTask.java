package com.gohn.kakaomobility.data;

import android.os.AsyncTask;
import android.util.Log;

import com.gohn.kakaomobility.etc.Constant;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gohn on 2018. 4. 27..
 */

public class CollectionParseTask extends AsyncTask<String, Void, Document> {
    private String uriPath;
    private CollectionParseListener listener;

    public CollectionParseTask(String uriPath, CollectionParseListener listener) {
        this.uriPath = uriPath;
        this.listener = listener;
    }

    public interface CollectionParseListener {
        void onCollectionsParsed(List<ThumbnailItem> photoItems);
    }

    @Override
    public void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    public Document doInBackground(String... params) {
        try {
            return Jsoup.connect(uriPath).get();
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void onPostExecute(Document document) {
        super.onPostExecute(document);

        if (document == null) {
            if (listener != null)
                listener.onCollectionsParsed(null);
        }

        // 콜렉션이 담긴 클래스를 가져온다.
        Elements elements = document.getElementsByClass("gallery-item-group exitemrepeater");

        if (elements != null) {
            List<ThumbnailItem> items = new ArrayList<>();

            // 링크 주소, 이미지 주소, 타이틀을 각각 가져온다.
            for (Element element : elements) {
                String link = element.select("a").first().attr("href");
                String resource = element.select("img").first().attr("src");
                String title = "unknown";

                Elements p = element.select("p");
                if (p != null && p.size() > 0) {
                    title = p.get(0).text();
                }

                // 객체로 담는다.
                ThumbnailItem item = new ThumbnailItem(title, link, resource);

                items.add(item);
            }

            // 리스너를 통해 Thumbnail 객체 리스트를 넘긴다.
            if (listener != null)
                listener.onCollectionsParsed(items);
        } else {
            if (listener != null)
                listener.onCollectionsParsed(null);
        }
    }
}
