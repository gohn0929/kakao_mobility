package com.gohn.kakaomobility.etc;

/**
 * Created by gohn on 2018. 4. 27..
 */

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.LruCache;
import android.widget.ImageView;

import com.gohn.kakaomobility.R;

import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// 사진 로드 하는 일을 하는 로더
public class ImageLoader {

    // 이미지가 다운로드 되는 동안 보여줄 샘플 이미지
    private int DEFAULT_PICTURE = R.mipmap.ic_launcher;

    // 액티비티를 가진다
    private Activity activity;

    // 사진의 주소를 키로 하고 리사이징한 이미지의 비트맵을 캐싱하기 위해 만든 캐시
    // https://developer.android.com/topic/performance/graphics/cache-bitmap
    private LruCache<String, Bitmap> memoryCache = new LruCache<>((int) (Runtime.getRuntime().maxMemory() / 1024) / 8);

    // 이미지 다운로드 풀링을 하기 위한 ExecutorService
    private ExecutorService executorService;

    public ImageLoader(Activity activity) {
        this.activity = activity;
        this.executorService = Executors.newFixedThreadPool(5);
    }

    // 어댑터에서 이미지를 뿌릴떄 이 함수를 이용한다
    public void display(ImageView imageView, String url) {

        Bitmap bitmap = memoryCache.get(url);

        // 이미지가 null 이 아니면 이미지를 셋팅하낟.
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        } else {
            // 캐싱된 비트맵 이미지가 없다면 이미지 맵에 이미지 뷰를 추가하고 이미지를 받기위해 큐에 넣는다
            queuePhoto(imageView, url);

            // 일단 기본 이미지를 출력한다
//            imageView.setImageResource(DEFAULT_PICTURE);
        }
    }

    private void queuePhoto(ImageView imageView, String url) {
        PictureToDraw p = new PictureToDraw(imageView, url);

        // 이미지 뷰와 거기에 뿌릴 이미지 주소를 하나의 클래스로 생성하여
        // 비트맵을 로드하는 로더에 일을 시킨다
        executorService.submit(new BitmapLoader(p));
    }

    // 로더가 일하게될 컨텐츠
    private class PictureToDraw {
        public ImageView imageView;
        public String url;

        public PictureToDraw(ImageView imageView, String url) {
            this.imageView = imageView;
            this.url = url;
        }
    }

    // Runnable 인터페이스를 구현한 비트맵 로더 클래스다
    private class BitmapLoader implements Runnable {
        PictureToDraw pictureToDraw;

        BitmapLoader(PictureToDraw pictureToDraw) {
            this.pictureToDraw = pictureToDraw;
        }

        @Override
        public void run() {
            // 웹에서 이미지를 가져온다
            Bitmap bmp = getBitmap(pictureToDraw.url);

            // 이 비트맵을 캐싱한다
            memoryCache.put(pictureToDraw.url, bmp);

            // 이 비트맵으로 Activity에서 돌릴 스레드를 구현한다
            BitmapDisplayer bd = new BitmapDisplayer(bmp, pictureToDraw);

            // 메인 액티비티의 ui 쓰레드에서 다운받은 이미지를 바로 적용시켜 준다
            activity.runOnUiThread(bd);
        }
    }

    // 웹에서 이미지를 가져오는 부분
    private Bitmap getBitmap(String src) {
        try {
            // url에서 비트맵을 가져온다
            return BitmapFactory.decodeStream((new URL(src)).openConnection().getInputStream());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    // Activity runonuithread에서 돌아갈 부분
    private class BitmapDisplayer implements Runnable {
        Bitmap bitmap;
        PictureToDraw pictureToDraw;

        public BitmapDisplayer(Bitmap bitmap, PictureToDraw pictureToDraw) {
            this.bitmap = bitmap;
            this.pictureToDraw = pictureToDraw;
        }

        public void run() {
            if (bitmap != null) {
                // 이미지가 정상적으로 존재하면 그 비트맵을 출력하고 아니면 기본 이미지를 출력한다
                pictureToDraw.imageView.setImageBitmap(bitmap);
            } else {
                pictureToDraw.imageView.setImageResource(DEFAULT_PICTURE);
            }
        }
    }
}